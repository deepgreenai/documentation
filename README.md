# deepgreen API documentation

Welcome to deepgreen's APIs documentation! We are excited to see what you will do with our models, and to get started quickly you will find below some instructions to make your first API calls.

This documentation is a work in progress and will be updated regularly. We will happily listen to your feedback to improve both the APIs and documentation. Reach out to us at [contact@deepgreen.ai](contact@deepgreen.ai).

## Authentication

All APIs currently only supports http basic (protected via ssl). Make sure to check your mailbox for your deepgreen credentials.
We will be working on additional authentication methods, for more details contact us at [contact@deepgreen.ai](contact@deepgreen.ai).

## Rate Limiting

We are still small, and we want to scale our infrastructure with your usage. We are introducing rate limiting to keep things stable, but we're happy to provide larger quotas quickly depending on your needs.

* API calls per hour: 30
* API calls per minute: 5
* API calls per second: 1

## Model APIs

The model APIs allows you to send an image to a specific model for analysis. The list of supported models can be accessed at https://api.deepgreen.ai/v1/models

### Usage

* HTTP POST to https://api.deepgreen.ai/v1/models/{model_name}[/versions/{model_version}]/prediction
* Path variables:
    * {model_name}: The name of the model to be called, see above table for the list of supported models.
    * {model_version} (OPTIONAL): The version of the model you want to call, see above table for the list of supported models.
* Query Parameters (OPTIONAL):
    * {confidence}: The minimum confidence threshold of the returned predictions. Values should be between 0 and 1. Defaults to 0.5.
* Request body:
    * Binary image data
* Headers:
    * Content-Type: application/octet-stream
    * Authorization: Basic dXNlcm5hbWU6cGFzc3dvcmQ=
        * Change dXNlcm5hbWU6cGFzc3dvcmQ= with your authorization value, calculated as follow: base64(username:password)
* Returned data:
    * model: Information about the model
        * name: The name of the model
        * version: The version of the model
        * classes: The list of all classes supported by the model
    * predictions: List of all detected patterns in the image
        * confidence: The confidence level of the prediction
        * prediction: The class being predicted
        * x1, x2: The top left relative coordinates of the predicted bounding box, between 0 and 1. To get the pixel coordinates, multiply x1 by the image width, and x2 by the image height.
        * y1, y2: The bottom right relative coordinates of the predicted bounding box, between 0 and 1. To get the pixel coordinates, multiply y1 by the image width, and y2 by the image height.

### Example response

```json
{
  "model": {
    "classes": [
      "spider_mite_damage",
      "powdery_mildew"
    ],
    "name": "cannabis_fisheye_pathogen_detection",
    "version": 2018091002
  },
  "predictions": [
    {
      "confidence": 0.9126214385032654,
      "prediction": "powdery_mildew",
      "x1": 0.6901707649230957,
      "x2": 0.702937662601471,
      "y1": 0.3166942596435547,
      "y2": 0.33342620730400085
    },
    {
      "confidence": 0.9012115001678467,
      "prediction": "powdery_mildew",
      "x1": 0.24147354066371918,
      "x2": 0.2539336383342743,
      "y1": 0.47684216499328613,
      "y2": 0.4939571022987366
    }
  ]
}
```

### Example usage with curl

The following code snippets upload a picture to the API over HTTP and receives a json response with deepgreen's analytics.

```bash
# Change "username" and "password" with your deepgreen API credentials, for more details contact us at contact@deepgreen.ai
declare USERNAME=username
declare PASSWORD=password
wget https://storage.googleapis.com/deepgreen_public/cannabis_light_mildew_raw.jpg
curl -X POST "https://api.deepgreen.ai/v1/models/plant_disease_detection_closeup/prediction" -u ${USERNAME}:${PASSWORD} -H "Content-Type: application/octet-stream" --data-binary "@cannabis_light_mildew_raw.jpg"
```

### Example usage with Java

Coming soon

### Example usage with Python

Coming soon

### react component

We provide a react component to easily display analytics over an image.

Your deepgreen credentials should be stored securely, so that they are never leaked to the frontend.

Coming soon.

### Other languages

We are happy to help! Let us know how, and we will happily provide you with more details on how to use our APIs from any platform.

## Error codes

HTTP Status

* 401: Authentication failure
* 429: Too many requests
